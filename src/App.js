import React, { useState,useEffect } from "react";
import Main from "./components/main";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./components/homePage/home";
import Create from "./components/homePage/create";
import Join from "./components/homePage/join";
import List from "./components/list";

const App = () => {

	/*this state contain all informations needed to render
	 * the list every time needed
	 * informations that goes here :
	 * - id of the form (also used for join link)
	 * -creator of the list form 
	 * -movies
	 *  votes for every movie
	 */
	const [listsInfo, setListsInfo] = useState([]);

	// this is the choosen form that is rendered in the list component 
	const [moviesForm ,setMoviesForm] = useState([])
	const getChoosenForm=(choosenForm )=>{
		setMoviesForm(choosenForm)
		console.log(moviesForm)
	}

    //this function add every new form created to the globalState
	const addForm = (newList) => {
		setListsInfo([...listsInfo, newList]);
	};
	
		

	return (
		<Router>
		<div>
			<Main />
				<Switch>
					<Route path="/create">
						<Create addForm={addForm} listInfo={listsInfo} />
					</Route>
					
						<Route path="/join">
						<Join getChoosenForm={getChoosenForm} finalForm={listsInfo} />
					</Route>
					
					<Route exact path="/">
						<Home />
					</Route>
			<Route path="/list" >

						<List setMoviesForm={setMoviesForm} moviesForm={moviesForm} />

					</Route>

				</Switch>
		</div>
			</Router>

	);
};

export default App;
