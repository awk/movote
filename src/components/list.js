import React, { useState, useEffect } from "react";
import App from "../App.js";
import MovieCard from "./listPage/movieCard";

const List = ({ moviesForm }) => {
	const [votingItems, setVotingItems] = useState([]);
	useEffect(() => {setVotingItems(moviesForm.movies )}, []);
	const handleUpvote = (itemId) => {
		const nextList = votingItems.map((item) => {
			if (item.id === itemId) {
				return Object.assign({}, item, {
					vote_count: item.vote_count + 1,
				});
			} else {
				return item;
			}
		});
		setVotingItems(nextList );
	};
	const handleDownvote = (itemId) => {
		const nextList = votingItems.map((item) => {
			if (item.id === itemId) {
				return Object.assign({}, item, {
					vote_count: item.vote_count - 1,
				});
			} else {
				return item;
			}
		});
		setVotingItems(nextList );
	};

	const items=votingItems.sort((a,b)=>(
		b.vote_count - a.vote_count
	))	
	return (
		<div>
			<h2>created by : {moviesForm.creator}</h2>
			{items
				? items.map((movie) => (
						<MovieCard
							movie={movie}
							votes={movie.vote_count}
							upvote={handleUpvote}
							downvote={handleDownvote}
						/>
				  ))
				: console.log("Add movies in the create page")}
		</div>
	);
};
export default List;
