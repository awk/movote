import React from "react";
import {
	FormControl,
	Button,
	Input,
	InputLabel,
	FormHelperText,
} from "@material-ui/core";
import {Link } from 'react-router-dom'

const Join = ({ finalForm ,getChoosenForm}) => {

	// check if the joinLink is valid and make it the choosen form that is rendered in list component
	// note : find method return undefined if no item in the list is valid 
	
	const handleJoin = ()=>{
		const joinLink = document.getElementById('join-input').value
		const ChoosenForm = finalForm.find(list => list.linkId == joinLink)
		if(ChoosenForm){
			getChoosenForm(ChoosenForm)
		}else{ 
			console.log("this id does not match any form !")
		}
	}

	return (
		<div>
			<FormControl>
				<InputLabel htmlFor="join-input" color="secondary">
					Join id
				</InputLabel>
				<Input
					name="link"
					required
					type="text"
					id="join-input"
					aria-describedby="join-helper-text"
				/>
				<FormHelperText id="join-helper-text">
					paste the join id here.
				</FormHelperText>
			</FormControl>
			<br />
			<FormControl>
				<InputLabel htmlFor="userName" color="secondary">
					User Name
				</InputLabel>
				<Input name="link" required type="text" id="userName" />
			</FormControl>
			<br />

			<Button
				variant="contained"
				onClick={() => handleJoin()}
				color="secondary"
				className="btns"
			>
				<Link to="/list">
					Join
				</ Link>
			</Button>
		</div>
	);
};

export default Join;
