import React from "react";

const SearchItem = ({setQuery , key, movie, addMovie }) => {
	const base_url = "https://image.tmdb.org/t/p/w45/";
	const { title, poster_path } = movie;

	const handleClick= (movie) => {
		addMovie(movie)
		//empty the search bar 
		setQuery({
			query: "",
			movies: []
		})
	};
	return (
		<div onClick={() => handleClick(movie)}>
			<h5>{title}</h5>
			<img src={`${base_url}${poster_path}`} />
		</div>
	);
};

export default SearchItem;
