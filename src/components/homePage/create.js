import React ,{useEffect}from "react";
import {
	FormControl,
	Button,
	Input,
	InputLabel,
	FormHelperText,
} from "@material-ui/core";
import "./create.css";
import SearchBar from "./searchBar";

const Create = ({ addForm, listInfo }) => {
	// this is where a single form is generated
	// this form should be added to the main state(listsForms)
	const form = {
		creator: "",
		movies: [],
		linkId: "",
	};

	const addMovie = (addedMovie) => {
		addedMovie.vote_count = 0
		form.movies.push(addedMovie);
		console.log(form.linkId);
	};
	// generate IDs for join components
	const validateForm = () => {
		const usrName = document.getElementById("user-input").value;
		if (usrName != "") {
			form.creator = usrName;
			console.log(listInfo)
		}
		else{
			form.creator = "unknown"
		}
		if (form.linkId == ""){
			form.linkId =Math.floor(Math.random() * 1000)
			alert(`your join id is ${form.linkId}`)
		}

		addForm(form)
		//add later if there is no movies in the state return false
	};
	return (
		<div>
			<FormControl>
				<InputLabel htmlFor="user-input" color="secondary">
					User Name
				</InputLabel>
				<Input
					name="userName"
					placeholder="abdallah khalaf"
					required
					type="text"
					id="user-input"
				/>
			</FormControl>
			<br />
			<SearchBar addMovie={addMovie} />
			<br />

			<Button
				variant="contained"
				color="secondary"
				className="btns"
				onClick={validateForm}
			>
				Create
			</Button>
		</div>
	);
};

export default Create;
