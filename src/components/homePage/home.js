import React from "react";
import {Paper} from "@material-ui/core";
import "../main.css";    
import background from "../../images/movie.jpg";    
    
export default function Home() {    
    const firstDescription = "create a form of movies you wanna try ";    
    const secondDescription = "vote for the best movie with your friends ! and enjoy watching ";    
    return (    
        <div className="second_container">    
            <Paper    
                children={firstDescription}    
                elevation={9}    
                className="description"    
            />    
            <img    
                src={background}    
                alt="movies pink picture"    
                className="bkg_img"    
            />    
            <Paper    
                children={secondDescription}    
                elevation={9}    
                className="description"    
            />    
        </div>    
    );    
}

