import React, { useState } from "react";
import {
	FormControl,
	Button,
	Input,
	InputLabel,
	FormHelperText,
} from "@material-ui/core";
import axios from 'axios'
import {APIKey , URL} from '../../requestConfig.js'
import SearchList from './searchList'

const SearchBar = ({addMovie}) => {
	// the query is the request parameter query
	// the movies is the list that shows on search
	const [query, setQuery] = useState({
		query: "",
		movies: [],
	});
	
	const handleInputChange = (e) => {
		e.preventDefault();
		const q = e.target.value.toLowerCase();
		setQuery({ query: q });
		q === ""
			? setQuery({movies: []})
			: axios
					.get(`${URL}${APIKey}&language=en-US&query=${q}&page=1`)
					.then((resp) => {
							resp.data.results.sort((movie1, movie2) => {
								const movie1Index = movie1.title
									.toLowerCase()
									.indexOf(q);
								const movie2Index = movie2.title
									.toLowerCase()
									.indexOf(q);
								if (movie1Index === -1 && movie2Index === -1)
									return 0;
								if (movie1Index === -1) return 1;
								if (movie2Index === -1) return -1;
								return movie1Index < movie2Index
									? -1
									: movie1Index > movie2Index
									? 1
									: 0;
							});
							setQuery({ movies: resp.data.results });
					})
					.catch((err) => setQuery({ movies: [] }));
		console.log(query.movies)};
	return (
		<FormControl>
			<InputLabel htmlFor="search-movies" color="secondary">
				Movies
			</InputLabel>
			<Input
				name="movies"
				placeholder="search for movies "
				required
				type="text"
				id="search-movies"
				value={query.query}
				onChange={(e)=>handleInputChange(e)}
				aria-describedby="search-help"
			/>
			<FormHelperText id="search-help">
				search here for your movies .
			</FormHelperText>
			<SearchList addMovie={addMovie} movies={query.movies} setQuery={setQuery} />		
		</FormControl>
	);
};

export default SearchBar;
