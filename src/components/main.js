import React from "react";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import "./main.css";
import background from "../images/movie.jpg";

const Main = () => {
	return (
		<div>
			<a className="logo" href="/">
				moVote
			</a>

			<div className="buttons_container">
				<Button
					variant="contained"
					color="secondary"
					className="btns"
					href="/create"
				>
					<Link to="/create">Create list</Link>
				</Button>
				<Button
					variant="contained"
					color="secondary"
					className="btns"
					href="/join"
				>
					<Link to="/join">Join Voting</Link>
				</Button>
			</div>
		</div>
	);
};

export default Main;
