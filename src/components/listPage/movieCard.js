import React,{useEffect} from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
	root: {
		maxWidth: 345,
	},
	media: {
		height: 140,
	},
});

export default function MovieCard({votes,upvote,downvote, movie }) {

	const handleUpvote = ()=>{
		upvote(movie.id)
	}
	const handleDownvote=()=>{
		downvote(movie.id)
	}
	
	const classes = useStyles();
	return (
		<Card className={classes.root} key={movie.id}>
			<CardActionArea>
				<CardMedia
					className={classes.media}
					image={`https://image.tmdb.org/t/p/original/${movie.backdrop_path}`}
					title="movies posters"
				/>
				<CardContent>
					<Typography gutterBottom variant="h5" component="h2">
						{movie.title}
					</Typography>
					<Typography
						variant="body2"
						color="textSecondary"
						component="p"
					>
						{movie.overview}
					</Typography>
				</CardContent>
			</CardActionArea>
			<CardActions>
				<Button size="small" color="primary" onClick={handleUpvote}>
					upvote
				</Button>
				<Button size="small" color="primary" onClick={handleDownvote}>
					downvote
				</Button>
			</CardActions>
			<div>votes : {votes}</div>
		</Card>
	);
}
